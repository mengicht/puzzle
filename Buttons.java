import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
 
public class Buttons extends JFrame {
 
    private static final int NUM_BUTTONS = 10;
    private MyButton[] buttons = new MyButton[NUM_BUTTONS];
    private JTextField displayField = new JTextField();
 
    /** button keeps track of its display value and its position */
    private class MyButton extends JButton {
        private int val;
        final private int pos;
 
        public MyButton(int v, int p)
        {
            pos = p;
            setVal(v);
        }
 
        public int getVal()
        {
            return val;
        }
 
        public int getPos()
        {
            return pos;
        }
 
        /** use s this to change the value on the button */
        public void setVal(int v)
        {
            val = v;
            setText(Integer.toString(val));
        }
    }
 
    public Buttons()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        JPanel p = new JPanel(new FlowLayout());
 
        ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    MyButton button = (MyButton) e.getSource();
                    doSomeStuff(button);
                }
        };
 
        for(int i = 0; i < NUM_BUTTONS; ++i) {
            MyButton b = new MyButton(i, i);
            b.addActionListener(listener);
            buttons[i] = b;
            p.add(b);
        }
 
        displayField = new JTextField(20);
        displayField.setEditable(false);
        displayField.setFont(new Font("Courier", Font.BOLD, 32));
        add(p, BorderLayout.CENTER);
        add(displayField, BorderLayout.NORTH);
        pack();
    }
 
    /**
     * Sets the displayField to value of the button clicked
     * and increments the value of its neighbor
     */
    private void doSomeStuff(MyButton b)
    {
        displayField.setText(Integer.toString(b.getVal()));
        int pos = b.getPos();
        if (pos + 1 < NUM_BUTTONS) {
            MyButton neighbor = buttons[pos + 1];
            int oldVal = neighbor.getVal();
            neighbor.setVal(oldVal + 1);
        }
    }
 
    public static void main(String[] args)
    {
        Buttons foo = new Buttons();
        foo.setVisible(true);
    }
}
