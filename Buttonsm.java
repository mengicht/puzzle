import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
 
public class Buttonsm extends JFrame {
 
    private static final int NUM_BUTTONS = 16;
    private MyButton[] buttons = new MyButton[NUM_BUTTONS];
    private JTextField displayField = new JTextField();
    private JPanel p = new JPanel();
    private JLabel label = new JLabel();
 
    /** button keeps track of its display value and its position */
    private class MyButton extends JButton {
        private int val;
        final private int pos;
 
        public MyButton(int v, int p)
        {
            pos = p;
            setVal(v);
        }
 
        public int getVal()
        {
            return val;
        }
 
        public int getPos()
        {
            return pos;
        }
 
        /** use s this to change the value on the button */
        public void setVal(int v)
        {
            val = v;
            setText(Integer.toString(val));
        }
    }
 
    public Buttonsm()
    {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        JPanel p = new JPanel(new GridLayout(4, 5));
 
        ActionListener listener = new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    MyButton button = (MyButton) e.getSource();
                    doSomeStuff(button);
                }
        };
 
        for(int i = 0; i < NUM_BUTTONS; ++i) {
        	
        	if (i == 0) {
        	
        		//label = new JLabel("");
                //p.addActionListener(listener);
        		p.add(label);
                //label.getPos();
        	}
        	else {
            MyButton b = new MyButton(i, i);
            b.addActionListener(listener);
            buttons[i] = b;
            p.add(b);
            }
        }
 
        displayField = new JTextField(20);
        displayField.setEditable(false);
        displayField.setFont(new Font("Courier", Font.BOLD, 32));
        add(p, BorderLayout.CENTER);
        add(displayField, BorderLayout.NORTH);
        pack();
        setVisible(true);
    }
 
    /**
     * Sets the displayField to value of the button clicked
     * and increments the value of its neighbor
     */
    private void doSomeStuff(MyButton b)
    {
        //String vaaalue = label.getPos();
        //System.out.println(vaaalue);
    	
    	displayField.setText(Integer.toString(b.getVal()));
        int pos = b.getPos();
        
        System.out.println(pos);
        int value = b.getVal();
        System.out.println(value);
        
        if (value == 1){
        	p.remove(label);
        	//p.add
        }
        p.revalidate(); /// this does not work
        p.validate();
        if ((pos + 1 == 0) || (pos - 1 == 0)) {
           
        	System.out.println("works");
        	MyButton neighbor = buttons[pos + 1];
        	MyButton thisbutton = buttons[pos];
            int oldVal = neighbor.getVal();
            neighbor.setVal(oldVal + 1);
            
           p.remove(thisbutton);
           System.out.println("works2");
        	
           p.validate();
            
        }
    }
    //set current position
    private void setCurrents(){
      /*  for (int i=0; i < this.getPos(); i++) {
            for (int j = 0; j < this.pos[i].length; j++) {
            	if(this.pos[i][j] == label){
            		this.currentX = j;
            		this.currentY = i;
            	}
            }
        }*/
    }
 
    public static void main(String[] args)
    {
        Buttonsm foo = new Buttonsm();
        //foo.
    }
}
