import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import java.awt.Robot;
import javax.swing.JPanel;


public class PuzzleOriginal extends JFrame implements ActionListener {

    private JPanel centerPanel;
    private JPanel southPanel;
    
    private JButton button;
    
    
    private JLabel label;
    private JButton exitButton;
    private JButton shuffleButton;
    
    private int currentX;
    private int currentY;
    
    int[][] pos;
    int width, height;

    public PuzzleOriginal() {

        pos = new int[][] {
                             {10, 1, 2, 3}, 
                            {4, 5, 6, 7}, 
                            {8, 9, 0, 11}, 
                            {12, 13, 14, 15}
                        };


        this.PuzzleFrame();               
        
        
    }
    public void PuzzleFrame() {
    	
        centerPanel = new JPanel();
        centerPanel.setLayout(new GridLayout(4, 4, 0, 0));
     
        add(Box.createRigidArea(new Dimension(0, 5)), BorderLayout.NORTH);    
        add(centerPanel, BorderLayout.CENTER);

        int counter = 0;
        for (int i=0; i < this.pos.length; i++) {
            for (int j = 0; j < this.pos[i].length; j++) {
                if ( pos[i][j] == 0) {
                    label = new JLabel("");
                    centerPanel.add(label);
                } else {
                	counter++;
                	System.out.print(j+ " j ");
                	System.out.println(i + " i ");
                	System.out.println(String.valueOf(pos[i][j])+ " pos i j ");
                	
                    button = new JButton();
                    button.setText(String.valueOf(pos[i][j]));
                    button.addActionListener(this);
                    centerPanel.add(button);
                    
                }
            }
        }
        
        

        
        
        
        setSize(new Dimension(425, 475));
        setTitle("Puzzle");
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        
        southPanel = new JPanel(new FlowLayout());
        JButton shuffleButton = new JButton("Shuffle");
        southPanel.add(shuffleButton);
        shuffleButton.addActionListener(this);
        
        exitButton =  new JButton("Exit");
        southPanel.add(exitButton);
        exitButton.addActionListener(this);
        
        add(southPanel, BorderLayout.SOUTH);
          
        
        setVisible(true);
        	
    }


    public static void main(String[] args) {

        new PuzzleOriginal();

    }
 

    public void actionPerformed(ActionEvent e) {
       
		
    	JButton button = (JButton) e.getSource();
    	
		//System.out.println(e.getSource());
    	if(button.getText() == "Exit"){
    		
    		// Closes system
    		System.exit(0); 
    		
    	}
    	if(button.getText() == "Shuffle"){
    		
    		
    		
    			/*pos = new int[][] {
                             {0, 1, 2, 3}, 
                             {4, 5, 6, 7}, 
                             {8, 9, 10, 11}, 
                             {12, 15, 14, 13}
                };*/
       for ( int m = 0; m < 16; m++) { 
                
                int random = (int)(Math.random() * (4) + 1);
				System.out.println(random + " random");
            
            	//this will attempt to run fifteen times 
            	// this will only run if there's room to move label DOWN
            
            	if (random == 1) {
            	
            	 outerloop:for (int i = 0; i < this.pos.length; i++) {
            	 	 for (int j = 0; j < this.pos[i].length; j++) {	
            		
            	 	 	 System.out.print(i + " i " + j + " j ");
            	 	 	 
            	 	 	 
					if ((pos[i][j] == 0) && ( i < 3 )) {
					   
						System.out.println(	 pos[i][j] + " position value" );
						
						// if "blank label" move DOWN   
						//int temp = pos[i][j];
						pos[i][j] = pos[i + 1][j];
						pos[i + 1][j] = 0;
						
						System.out.println(	 pos[i][j] + " NEW position value" );
                	
						//i = 4;
						  break outerloop;
                		}
                	 }
                   }
                }
               else if (random == 2) {
               	   
                outerloop:for (int i = 0; i < this.pos.length; i++) {
                	for (int j = 0; j < this.pos[i].length; j++) {	
                		
            	 	 	 System.out.print(i + " i " + j + " j ");
            	
                	if ((pos[i][j] == 0) && ( i > 0 )) {
						
                		System.out.println(	 pos[i][j] + " position value" );
						
						// if "blank label" move UP   
					//	int temp = pos[i][j];
						pos[i][j] = pos[i - 1][j];
						pos[i - 1][j] = 0;
						
						System.out.println(	 pos[i][j] + " NEW position value" );
						break outerloop;
                	}	
                	}
                }
                }
                else if (random == 3) {
                	
                 outerloop:for (int i = 0; i < this.pos.length; i++) {
                	for (int j = 0; j < this.pos[i].length; j++) {	
                		
                		
            	 	 	 System.out.print(i + " i " + j + " j ");
            	
                	if ((pos[i][j] == 0) && ( j > 0 )) {
					   
						System.out.println(	 pos[i][j] + " position value" );
						
						// if "blank label" move RIGHT   
						//int temp = pos[i][j];
						pos[i][j] = pos[i][j - 1];
						pos[i][j - 1] = 0;
						
						System.out.println(	 pos[i][j] + " NEW position value" );
						break outerloop;
                	}		
                	}
                 }
                }
                else  {
                	
                 outerloop:for (int i = 0; i < this.pos.length; i++) {
                	for (int j = 0; j < this.pos[i].length; j++) {	
                		
            	 	 	 System.out.print(i + " i " + j + " j ");
            	
                	if ((pos[i][j] == 0) && ( j < 3 )) {
					   
						System.out.println(	 pos[i][j] + " position value" );
						
						// if "blank label" move LEFT   
						//int temp = pos[i][j];
						pos[i][j] = pos[i][j + 1];
						pos[i][j + 1] = 0;
						
						System.out.println(	 pos[i][j] + " NEW position value" );
						break outerloop;
                	}	
                		
                }
               }
              }
       		}// end 
                
                
                    
               
                
    		 this.PuzzleFrame();          
    	
        
    	System.out.println(button.getText());
    	
        }
        
        // Actual movement of the buttons 
        
        else {
        	
        	
        
    	
    	
    	//int labelX = label.getX();
		//int labelY = label.getY();
		
		//Dimension sizeLabel = label.getSize();

		//int labelPosX = labelX / sizeLabel.width;
		//int labelPosY = labelY / sizeLabel.height;
		
		//int labelIndex = pos[labelPosX][labelPosY];
		
			

       	Dimension size = button.getSize();

//        int labelX = label.getX();
//        int labelY = label.getY();
        
        int buttonX = button.getX();
        int buttonY = button.getY();
        
        int buttonPosX = buttonX / size.width; // 15 // 3
        int buttonPosY = buttonY / size.height; // 15 // 3
        
        int buttonIndex = pos[buttonPosY][buttonPosX]; // place where button is on int Index
        
         System.out.println(pos[buttonPosY][buttonPosX] + " 1button Position? WORKS");
      
        //int buttonIndex = 0;
       
         this.setCurrents(buttonIndex); // this gets index of b clicked
         int labelX = this.currentX;
         int labelY = this.currentY;
         
         int labelIndex = pos[labelX][labelY];
         
         System.out.println(pos[labelX][labelY] +  " labelIndex");
         
         System.out.println("labelX= " + labelX + "labelY " + labelY); // does not work :( 1 less????
         
         
         
         this.setCurrents(0); //this gets index of 0 (label)
         int zeroX = this.currentX;
         int zeroY = this.currentY;
         
         System.out.println(pos[zeroX][zeroY] +  " zero!!!Index"); // 0?
         
         System.out.println("lableX= " + zeroX + "labelY " + zeroX); // 2 / 2 works!!!!! 
         
        // buttonIndex = this.pos[labelX][labelY];
         
         //this.pos[labelX][labelY] = pos[zeroX][zeroY];
         this.pos[buttonPosY][buttonPosX] = 0; //pos[zeroX][zeroY];
         
         this.pos[zeroX][zeroY] = buttonIndex;  
         
         
         	// if "blank label" move DOWN   
         	//int temp = pos[i][j];
			//pos[i][j] = pos[i + 1][j];
			//pos[i + 1][j] = 0;
						
						
         
         System.out.println(this.pos[labelX][labelY] + " button Position?"); // does not work
         
         System.out.println( this.pos[zeroX][zeroY] + " label Position?"); // works
         
         System.out.println(buttonIndex + "buttonINdex"); 
         
         
         this.PuzzleFrame();
         this.validate();
        // if buttons are same horizontal AND label X is to the RIGHT of button  
//        if (labelY == buttonY && (labelX - buttonX) == size.width ) {

             /*int labelIndex = buttonIndex + 1;
             
             
             System.out.println(labelIndex + "labelIndex");

             centerPanel.remove(buttonIndex);
             System.out.println("removed!");
             
             centerPanel.add(label, buttonIndex);
             
             System.out.println("added");
             centerPanel.add(button,labelIndex);

             System.out.println("added 2");
             centerPanel.validate();
             */

             System.out.println(buttonIndex + "buttonINdex");
           //  this.PuzzleFrame();
             
             
        }
               
//             
//        }
//         // if buttons are same horizontal AND label X is to the LEFT of button  
//        if (labelY == buttonY && (labelX - buttonX) == -size.width ) {
//        	/*
//             int labelIndex = buttonIndex - 1;
//
//             centerPanel.remove(buttonIndex);
//             centerPanel.add(label, buttonIndex);
//             centerPanel.add(button,labelIndex);
//             */
//             System.out.println(buttonIndex + "buttonINdex");
//              this.PuzzleFrame();
//             
//             //centerPanel.validate();
//        
//        }
//        
//        
//        // if buttons are the same vertical and label is UNDER button
//        if (labelX == buttonX && (labelY - buttonY) == size.height ) {
//
//             int labelIndex = buttonIndex + 4;
//
//             centerPanel.remove(buttonIndex);
//             centerPanel.add(label, buttonIndex);
//             centerPanel.add(button,labelIndex);
//             //centerPanel.validate();
//             
//             this.PuzzleFrame();
//        }
//        // if buttons are the same vertical and label is ABOVE button
//        if (labelX == buttonX && (labelY - buttonY) == -size.height ) {
//
//             int labelIndex = buttonIndex - 4;
//             centerPanel.remove(labelIndex);
//             centerPanel.add(button,labelIndex);
//             centerPanel.add(label, buttonIndex);
//             //centerPanel.validate();
//             
//             this.PuzzleFrame();
//       }


        
       
        
    
    	
    }
    public void setCurrents(int label){
        for (int i = 0; i < this.pos.length; i++) {
            for (int j = 0; j < this.pos[i].length; j++) {
            	if(this.pos[i][j] == label){
            		this.currentX = j;
            		this.currentY = i;
            		
            	}
            }
        }
    }
//    public void swap(int x, int y, int newX, int newY){
//    	this.pos[x][y]
//    }
}
